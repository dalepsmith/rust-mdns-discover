
TARGET := x86_64-pc-windows-gnu

APP := target/$(TARGET)/release/rust-mdns-discover.exe
VER := $(shell cargo metadata --format-version=1 --no-deps | jq -r .packages[0].version)

all: $(APP)

$(APP): src/main.rs
	cargo build --release --target $(TARGET)

clean::
	cargo clean

# For testing on Linux
# USRDIR := $$HOME/.mozilla/native-messaging-hosts
# install-local: all mdns.discover.firefox.json.in
# 	mkdir -p $(USRDIR)
# 	sed -e "s|@DIR@|$(USRDIR)|" mdns.discover.firefox.json.in > $(USRDIR)/mdns.discover.json
# 	cp -f $(APP) $(USRDIR)/

# uninstall-local:
# 	$(RM) -f $(USRDIR)/mdns.discover.json
# 	$(RM) -f $(USRDIR)/rust-mdns-discover

# LIBDIR = /usr/lib/mozilla/native-messaging-hosts
# install: all mdns.discover.firefox.json.in
# 	mkdir -p $(LIBDIR)
# 	sed -e "s|@DIR@|$(LIBDIR)|" mdns.discover.firefox.json.in > $(LIBDIR)/mdns.discover.json
# 	cp -f $(APP) $(LIBDIR)/

# uninstall:
# 	$(RM) -f $(LIBDIR)/rust-mdns-discover
# 	$(RM) -f $(LIBDIR)/mdns.discover.json


MSI=rust-mdns-discover-$(VER).msi
installer: $(MSI)

# See https://subscription.packtpub.com/book/application-development/9781782160427/1/ch01lvl1sec09/your-first-wix-project
$(MSI): $(APP) mdns.discover.firefox.json.in rust-mdns-discover.wxs.in
	cp $(APP) .
	sed -e "s|@DIR@/rust-mdns-discover|rust-mdns-discover.exe|" mdns.discover.firefox.json.in > mdns.discover.json
	sed -e "s|@VER@|$(VER)|" rust-mdns-discover.wxs.in > rust-mdns-discover.wxs
	wixl -v -o $@ rust-mdns-discover.wxs

clean::
	-rm *.msi *.exe mdns.discover.json rust-mdns-discover.wxs
