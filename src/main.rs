#![warn(clippy::pedantic, clippy::nursery, clippy::unwrap_used)]
//#![warn(clippy::expect_used)]
use chrono::prelude::*;
use mdns_sd::{Receiver, ServiceDaemon, ServiceEvent, ServiceInfo};
use serde::Serialize;
use std::collections::HashMap;
use std::io;
use std::io::prelude::*;

// A response looks like either:
// {
//     type: 'remove',
//     service: 'Service Name'
// }
//
// Or:
// {
//     type: 'add',
//     service: 'Service Name',
//     srv: '_http._tcp'
//     hostname: 'name.local',
//     address: '192.168.10.20',
//     port: 80,
//     txt: {
//         'key1': 'Value one',
//         'key2': null,
//         ...
//         'keyN': 'Value N'
//     }
// }

#[derive(Debug, Serialize)]
#[serde(tag = "type")]
pub enum ResponseJSON {
    #[serde(rename = "remove")]
    Remove { service: String },

    #[serde(rename = "add")]
    Add {
        service: String,
        srv: String,
        hostname: String,
        address: String,
        port: u16,
        txt: HashMap<String, Option<String>>,
    },
}

// https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging#app_side

trait Rw {
    fn read(&self) -> Option<String>;
    fn write(&self, s: &str);
}

struct Text;
impl Rw for Text {
    fn read(&self) -> Option<String> {
        let mut stdin = io::stdin().lock();
        let mut line = String::new();
        stdin.read_line(&mut line).ok().filter(|&len| len > 0)?;
        Some(line.trim().to_string())
    }
    fn write(&self, s: &str) {
        println!("{} = {}", Local::now().format("%H:%M:%S.%6f"), s);
    }
}

struct Framed;
impl Rw for Framed {
    fn read(&self) -> Option<String> {
        let mut stdin = io::stdin().lock();
        let mut count = [0_u8; 4];
        stdin.read_exact(&mut count).ok()?;

        // Actual limit is 4GB, but we should typically get about 12: `"_http._tcp"`
        let mut buffer = vec![0_u8; u32::from_ne_bytes(count) as usize];
        stdin.read_exact(&mut buffer).ok()?;

        serde_json::from_slice(&buffer).ok()
    }

    fn write(&self, s: &str) {
        // Actual limit is 1M
        let count: u32 = u32::try_from(s.len()).expect("JSON length");
        let mut stdout = io::stdout().lock();
        stdout.write_all(&count.to_ne_bytes()).expect("Write count");
        stdout.write_all(s.as_bytes()).expect("Write JSON");
        stdout.flush().expect("Flush");
    }
}
fn fullname_to_service(fullname: &str) -> String {
    // "Service Name._my-service._udp.local." -> "Service Name"
    fullname
        .rsplitn(5, '.')
        .nth(4)
        .expect("Not enough dots in fullname")
        .to_string()
}

fn fulltype_to_srv(fulltype: &str) -> String {
    // "_my-service._udp.local." -> "_my-service._udp"
    fulltype
        .rsplitn(3, '.')
        .nth(2)
        .expect("Not enough dots in type")
        .to_string()
}

fn add_message(info: &ServiceInfo) -> ResponseJSON {
    ResponseJSON::Add {
        service: fullname_to_service(info.get_fullname()),
        srv: fulltype_to_srv(info.get_type()),
        hostname: info.get_hostname().to_string(),
        port: info.get_port(),
        address: info
            .get_addresses()
            .iter()
            .next()
            .map_or(String::new(), ToString::to_string),
        txt: info
            .get_properties()
            .iter()
            .map(|t| {
                (
                    t.key().to_string(),
                    t.val().map(|s| String::from_utf8_lossy(s).to_string()),
                )
            })
            .collect(),
    }
}

fn remove_message(fullname: &str) -> ResponseJSON {
    ResponseJSON::Remove {
        service: fullname_to_service(fullname),
    }
}

fn main() {
    // Arguments will be:
    // 0  app name
    // 1  complete path to manifest
    // 2  ID of addon that initiated

    // For Example:
    // (0, "/usr/lib/mozilla/native-messaging-hosts/rust-mdns-discover")
    // (1, "/usr/lib/mozilla/native-messaging-hosts/mdns.discover.json")
    // (2, "mdns@exmaple.com")

    let rw: &(dyn Rw + Send + Sync) = if std::env::args().len() > 1 {
        &Framed
    } else {
        &Text
    };

    let mdns = ServiceDaemon::new().expect("Failed to create daemon");
    let mut maybe_service = rw.read();
    while let Some(service) = maybe_service {
        let service_name = service.clone() + ".local.";
        let receiver = mdns.browse(&service_name).expect("Start browse");
        let thread = std::thread::spawn(move || handler(rw, &receiver));
        maybe_service = rw.read();
        mdns.stop_browse(&service_name).expect("Stop browse");
        thread.join().ok();
    }
    mdns.shutdown().ok();
}

fn handler(rw: &dyn Rw, receiver: &Receiver<ServiceEvent>) {
    while let Ok(event) = receiver.recv() {
        if let Some(message) = match event {
            ServiceEvent::ServiceResolved(info) => Some(add_message(&info)),
            ServiceEvent::ServiceRemoved(_type, fullname) => Some(remove_message(&fullname)),
            _ => None,
        } {
            if let Ok(encoded) = serde_json::to_string(&message) {
                rw.write(&encoded);
            }
        }
    }
}
